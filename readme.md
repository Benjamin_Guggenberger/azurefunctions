# Get Repository (switch to master branch)
https://gitlab.com/Benjamin_Guggenberger/azurefunctions.git

# Example Usages (Disclaimer: as I have only a free account for one month, those urls will probably not be available anymore)
CreateCustomer:
https://sfrfunctionapp.azurewebsites.net/api/CreateCustomer
{ "IBAN":"AT20-2200-0022-3333-9364", "Name":"Fritz Truthahn", "Address":"Baumstraße 49"}

GetCustomer:
https://sfrfunctionapp.azurewebsites.net/api/Customer?iban=AT20-2200-0022-3333-4444

CreateTransaction:
https://sfrfunctionapp.azurewebsites.net/api/CreateTransaction
{ "Amount":"200", "Description":"Festival", "CreditorIBAN":"AT20-2200-0022-3333-4444", "DebtorIBAN":"AT20-2200-0022-3333-0000"}

GetMonthlyReport:
https://sfrfunctionapp.azurewebsites.net/api/GetMonthlyReport?iban=AT20-2200-0022-3333-4444&month=3