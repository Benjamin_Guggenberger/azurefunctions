using System.Collections.Generic;

namespace AzureFunctions
{
    public class Report
    {
        public Customer Customer { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
}