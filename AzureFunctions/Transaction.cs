using System;
using Microsoft.Azure.Cosmos.Table;

namespace AzureFunctions
{
    public class Transaction : TableEntity
    {
        public int Amount { get; set; }
        public DateTime ExecutionDate { get; set; }
        public string Description { get; set; }
        public string CreditorIBAN { get; set; }
        public string DebtorIBAN { get; set; }
    }
}