using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AzureFunctions.Functions
{
    public static class CreateCustomer
    {
        [FunctionName("CreateCustomer")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            [Table("Customer", Connection = "sfrazurefunctionstorage_STORAGE")] IAsyncCollector<Customer> customerCollector, ILogger log)
        {
            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var input = JsonConvert.DeserializeObject<Customer>(requestBody);

            input.RowKey = input.IBAN;
            input.PartitionKey = "Customers";

            await customerCollector.AddAsync(input);

            log.LogInformation($"Added Customer with IBAN {input.IBAN}.");

            return new OkObjectResult(input.IBAN);
        }
    }
}
