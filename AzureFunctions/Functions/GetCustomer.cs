using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;

namespace AzureFunctions.Functions
{
    public static class GetCustomer
    {
        [FunctionName("GetCustomer")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "Customer")] HttpRequest req,
            [Table("Customer", Connection = "sfrazurefunctionstorage_STORAGE")] CloudTable customerTable,
            ILogger log)
        {
            string iban = req.Query["iban"];

            var operation = TableOperation.Retrieve<Customer>("Customers", iban);
            var customer = await customerTable.ExecuteAsync(operation);

            return new OkObjectResult(customer.Result);
        }
    }
}
