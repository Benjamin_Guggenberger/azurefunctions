using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;

namespace AzureFunctions.Functions
{
    public static class GetMonthlyReport
    {
        [FunctionName("GetMonthlyReport")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            [Table("Customer", Connection = "sfrazurefunctionstorage_STORAGE")] CloudTable customerTable,
            [Table("Transaction", Connection = "sfrazurefunctionstorage_STORAGE")] CloudTable transactionTable,
            ILogger log)
        {
            string iban = req.Query["iban"];
            int month = Convert.ToInt32(req.Query["month"]);

            var operation = TableOperation.Retrieve<Customer>("Customers", iban);
            var tableResult = await customerTable.ExecuteAsync(operation);
            var customer = (Customer)tableResult.Result;

            TableQuery<Transaction> query = new TableQuery<Transaction>()
                .Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition(nameof(Transaction.CreditorIBAN), QueryComparisons.Equal, iban),
                    TableOperators.Or,
                    TableQuery.GenerateFilterCondition(nameof(Transaction.DebtorIBAN), QueryComparisons.Equal, iban)));

            var segment = await transactionTable.ExecuteQuerySegmentedAsync(query, null);
            var transactions = segment.Results.Where(transaction => transaction.ExecutionDate.Month == month).ToList();

            var report = new Report
            {
                Customer = customer,
                Transactions = transactions
            };

            return new OkObjectResult(report);
        }
    }
}
