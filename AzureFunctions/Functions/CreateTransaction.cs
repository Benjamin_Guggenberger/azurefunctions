using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AzureFunctions.Functions
{
    public static class CreateTransaction
    {
        [FunctionName("CreateTransaction")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            [Table("Transaction", Connection = "sfrazurefunctionstorage_STORAGE")] IAsyncCollector<Transaction> transactionCollector, ILogger log)
        {
            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var input = JsonConvert.DeserializeObject<Transaction>(requestBody);

            input.RowKey = Guid.NewGuid().ToString();
            input.PartitionKey = "Customers";
            input.ExecutionDate = DateTime.UtcNow;

            await transactionCollector.AddAsync(input);

            log.LogInformation($"Added Transaction with Description {input.Description}.");

            return new OkObjectResult(input.Description);
        }
    }
}
