using Microsoft.Azure.Cosmos.Table;

namespace AzureFunctions
{
    public class Customer : TableEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string IBAN { get; set; }
    }
}